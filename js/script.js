var state;

randomImage(fadeIn);

$('.image').click(function(){
	randomSound().play();
	fadeOut(function(){
		randomImage(fadeIn);
	});
});

$('.button').click(function(){
	if (state) {
		$('.about').fadeOut(1000, function(){
			randomImage(fadeIn);
			$('.button').html("&plus;");
			state = false;
		});
	} else {
		fadeOut(function(){
			$('.about').fadeIn(1000);
			$('.button').html("&times;");
			state = true;
		});
	}
})

function randomImage(callback) {
	let n = Math.floor(Math.random() * 1000) + 1 + "";
   n = n.padStart(4, '0');
   let source = "images/seed" + n + ".png";
   let img = new Image();
   img.src = source;
   $('.image').attr("src", source);
   img.onload = callback;
}

function randomSound() {
	let array = $('audio');
	let audio = array[Math.floor(Math.random()*array.length)];
	return audio;
}

function fadeIn() {
	$('.image').fadeIn(1000);
}

function fadeOut(after) {
	$('.image').fadeOut(1000, after);	
}