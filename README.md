# ezabusonemletezik.gitlab.io

**Ez a Busó nem létezik** - _neurális hálózati modell weboldal-dokumentációja_

Az "Ez a Busó nem létezik" egy neurális hálózati modell, amely egy tanulási algoritmus útján képes nemlétező Busóálarcok generálására.

Ez a mappa tartalmazza az "Ez a Busó nem létezik" című munka dokumentációját egy weboldal formájában, valamint ennek részeként 1000 db 1024x1024 pixeles, a neurális hálózat által generált képet (lásd: "images" mappa).

Bácsi Barnabás, 2021
